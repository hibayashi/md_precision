 # Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Do not do stty when it is not a tty
tty > /dev/null 2> /dev/null
if [ "$?" -eq 0 ]; then
    stty erase ^?
    stty intr ^C
fi

# prevent sighup from killing background jobs
trap "" SIGHUP

# for emacs shell mode:
[ -n "$PS1" -a -n "$TERM" ] && {
    # Under emacs, keep tty sane. also disable more/less.
    [ -n "${EMACS:-}" ] && {
        stty  -icrnl -onlcr -echo susp ^Z
        export PAGER=cat
        export TERM=emacs
    }
}
# Set personal aliases

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home1/ibayashi/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home1/ibayashi/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home1/ibayashi/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home1/ibayashi/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
export PATH="/home1/ibayashi/anaconda3/bin:$PATH"  # commented out by conda initialize 

module load gcc/8.3.0
module load cuda/11.1-1
conda activate jaxmd_sam 