import matplotlib.pyplot as plt
import numpy as np
from numpy import linalg as LA

SAM_forces = np.load("SAM_forces.npy")
sam_res = []
for i in range(100):
    cnt = 0
    for j in range(64):
        if LA.norm(SAM_forces[i, j]) > 1e-3:
            cnt += 1
    sam_res.append(cnt)
Adam_forces = np.load("Adam_forces.npy")
adam_res = []
for i in range(100):
    cnt = 0
    for j in range(64):
        if LA.norm(Adam_forces[i, j]) > 1e-3:
            cnt += 1
    adam_res.append(cnt)
plt.plot(adam_res, label="adam")
plt.plot(sam_res, label="sam")
plt.legend()

plt.xlabel("Time")
plt.ylabel("Unphysical Atom #")
plt.show()
