import training
import simulation

if __name__ == "__main__":
    training.run(with_SAM=False)
    simulation.run()
