from turtle import position
from jax import random
import numpy as np
from jax import lax
import jax
import jax.numpy as jnp
from jax_md import space, energy, quantity, simulate
import pickle
from jax.config import config
config.update("jax_enable_x64", True)

# Config
key, split = random.split(random.PRNGKey(0))
write_every = 100
dt = 5e-3
steps = 10000

# Box properties
BOX_SIZE = 10.862


def run(model_fn: str):
    with open(model_fn, 'rb') as input:
        params = pickle.load(input)
    displacement_fn, shift_fn = space.periodic(BOX_SIZE)
    init_fn, gnn_energy_fn = energy.graph_network(
        displacement_fn, r_cutoff=3.0)

    def kT(t):
        return jnp.where(t < 5000.0 * dt, 0.1, 0.01)

    def energy_fn(R, kT=None):
        # kT is not needed (JAXMD Bug)
        return gnn_energy_fn(params, R)

    init_R = np.load("./data/Rs_0000.npy")

    init, apply = simulate.nvt_nose_hoover(energy_fn, shift_fn, dt, kT(0.))

    def step_fn(i, state_and_log):
        state, log = state_and_log

        t = i * dt
        # Log information about the simulation.
        T = quantity.temperature(state.velocity)
        log['kT'] = log['kT'].at[i].set(T)
        H = simulate.nvt_nose_hoover_invariant(energy_fn, state, kT(t))
        log['H'] = log['H'].at[i].set(H)
        # Record positions every `write_every` steps.
        log['position'] = lax.cond(i % write_every == 0,
                                   lambda p:
                                   p.at[i // write_every].set(state.position),
                                   lambda p: p,
                                   log['position'])
        # Take a simulation step.
        state = apply(state, kT=kT(t))
        return state, log
    state = init(key, init_R)

    log = {
        'kT': jnp.zeros((steps,)),
        'H': jnp.zeros((steps,)),
        'position': jnp.zeros((steps // write_every,) + init_R.shape)
    }

    state, log = lax.fori_loop(0, steps, step_fn, (state, log))
    grad_fn = jax.grad(gnn_energy_fn, argnums=1)
    forces = np.zeros((100, 64, 3))
    for i in range(100):
        forces[i, :, :] = -grad_fn(params, log['position'][i, :, :])
    np.save("SAM_forces", forces)


if __name__ == "__main__":
    run(model_fn='gnn_params_epoch_30_SAM.pkl')
