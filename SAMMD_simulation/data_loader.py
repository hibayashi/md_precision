from torch.utils.data import Dataset
import numpy as np
import os
import glob


class REFData(Dataset):
    def __init__(self, fpath, is_train):
        self.root_dir = fpath  # load root dir
        self.is_train = is_train

    def __len__(self):
        """
          return total dataset size assuming no. of E, R, & F frames are the same
        """
        files = self.root_dir+'/test_Rs_*.npy'
        if self.is_train:
            files = self.root_dir+'/Rs_*.npy'
        files_list = [f for f in glob.glob(files)]
        return len(files_list)

    def __getitem__(self, index):
        pos_file_name = 'Rs_'+str(index).zfill(4)+'.npy'
        pos_file_path = os.path.join(self.root_dir, pos_file_name)
        force_file_name = 'Fs_'+str(index).zfill(4)+'.npy'
        force_file_path = os.path.join(self.root_dir, force_file_name)
        energy_file_name = 'Es_'+str(index).zfill(4)+'.npy'
        energy_file_path = os.path.join(self.root_dir, energy_file_name)

        Rs_numpy = np.load(pos_file_path)
        Fs_numpy = np.load(force_file_path)
        Es_numpy = np.load(energy_file_path)
        return Rs_numpy, Fs_numpy, Es_numpy
