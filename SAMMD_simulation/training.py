from optax._src import numerics
from jax.tree_util import tree_flatten, tree_unflatten
from jax import random
from data_loader import REFData
import numpy as np
from torch.utils.data import Dataset, DataLoader
import seaborn as sns
import jax
import jax.numpy as jnp
import optax
from jax_md import space, energy
from jax import vmap
import pickle

# Box properties
BOX_SIZE = 10.862

# Trainig properties
TRAIN_BATCH_SIZE = 4
TRAIN_NUM_WORKERS = 1

# Test properties
TEST_BATCH_SIZE = 500
TEST_NUM_WORKERS = 1

# SAM parameters
RHO = 0.05
LEARNING_RATE = 1e-4
optim = optax.adam(LEARNING_RATE)
EPOCHS = 30


# Get training set and test data set and create data generators for batch training
training_set = REFData('./data/', True)
testing_set = REFData('./data/', False)

training_generator = DataLoader(
    training_set,
    shuffle=True,
    batch_size=TRAIN_BATCH_SIZE,
    num_workers=TRAIN_NUM_WORKERS,
)

testing_generator = DataLoader(
    testing_set,
    shuffle=False,
    batch_size=TEST_BATCH_SIZE,
    num_workers=TEST_NUM_WORKERS,
)


# Get params for network and start training.
key = random.PRNGKey(0)

displacement_fn, shift_fn = space.periodic(BOX_SIZE)

init_fn, energy_fn = energy.graph_network(displacement_fn, r_cutoff=3.0)
vectorized_energy_fn = vmap(energy_fn, (None, 0))


def energy_loss_fn(params, batch):
    return jnp.mean((vectorized_energy_fn(params, batch['Rs']) - batch['Es']) ** 2)


def force_loss_fn(params, batch):
    grad_fn = vmap(jax.grad(energy_fn, argnums=1), (None, 0))
    return jnp.mean((grad_fn(params, batch['Rs']) + batch['Fs']) ** 2)


@jax.jit
def loss_fn(params, batch):
    return energy_loss_fn(params, batch) + force_loss_fn(params, batch)


@jax.jit
def sam_train_step(params, opt_state, batch):
    grad_fn = jax.grad(loss_fn, has_aux=False)
    grads = grad_fn(params, batch)

    norm = jnp.sqrt(
        sum([jnp.sum(numerics.abs_sq(x)) for x in jax.tree_leaves(grads)]))

    gradient_flat, gradient_tree = tree_flatten(grads)
    e_w_flat = [(x*RHO)/norm for x in gradient_flat]
    params_flat, params_tree = tree_flatten(params)
    sharp_params_flat = jax.tree_map(
        lambda x, y: x+y, params_flat, e_w_flat)
    sharp_params = tree_unflatten(params_tree, sharp_params_flat)
    sam_grads = grad_fn(sharp_params, batch)

    updates, new_opt_state = optim.update(sam_grads, opt_state, params)
    new_params = optax.apply_updates(params, updates)
    return new_params, new_opt_state


@jax.jit
def adam_train_step(params, opt_state, batch):
    grad_fn = jax.grad(loss_fn, has_aux=False)
    grads = grad_fn(params, batch)
    updates, new_opt_state = optim.update(grads, opt_state, params)
    new_params = optax.apply_updates(params, updates)
    return new_params, new_opt_state


def run(with_SAM: bool):
    # Get params and start training
    params = init_fn(key,  training_set[0][0])

    opt_state = optim.init(params)

    for epoch in range(EPOCHS):
        print(f'Epoch {epoch} Started ... ', end="")
        for training_data in training_generator:
            batch = {'Rs': jnp.array(training_data[0]),
                     'Fs': jnp.array(training_data[1]),
                     'Es': jnp.array(training_data[2])}
            if with_SAM:
                params, opt_state = sam_train_step(params, opt_state, batch)
            else:
                params, opt_state = adam_train_step(params, opt_state, batch)
        print(f'Done.', )
        train_loss = []
        test_loss = []
        for training_data in training_generator:
            batch = {'Rs': jnp.array(training_data[0]),
                     'Fs': jnp.array(training_data[1]),
                     'Es': jnp.array(training_data[2])}
            train_loss.append(loss_fn(params, batch))
        for test_data in testing_generator:
            batch = {'Rs': jnp.array(test_data[0]),
                     'Fs': jnp.array(test_data[1]),
                     'Es': jnp.array(test_data[2])}
            test_loss.append(loss_fn(params, batch))
        print(f"\tTrain Loss:\t{np.mean(train_loss)}")
        print(f"\tTest Loss: \t{np.mean(test_loss)}")

    with open(f'gnn_params_epoch_{EPOCHS}.pkl', 'wb') as out:
        pickle.dump(params, out)


if __name__ == "__main__":
    run(with_SAM=True)
