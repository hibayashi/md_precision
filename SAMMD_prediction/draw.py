import matplotlib.pyplot as plt
import numpy as np
trajectory = np.load("results/trajectory_SAM.npy")
epochs = trajectory[:,0]
train_loss = trajectory[:,1]
test_loss = trajectory[:,2]
plt.style.use('classic')
plt.plot(epochs, train_loss, label="Train Loss")
plt.plot(epochs, test_loss, label="Test Loss")
plt.ylabel('Loss (Energy + Force)')
plt.xlabel('Epochs')
plt.legend(loc="upper right")
plt.savefig("plot.png")
