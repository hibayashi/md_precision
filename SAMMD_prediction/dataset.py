import torch
import jax.numpy as jnp

class Dataset(torch.utils.data.Dataset):
  def __init__(self, is_train):
    self.is_train = is_train
    if self.is_train:
      self.IDs = jnp.arange(2416)
    else:
      self.IDs = jnp.arange(1302)

  def __len__(self):
    return len(self.IDs)

  def __getitem__(self, index):
    if self.is_train:
      R = torch.load('data/Rs_' + str(index).zfill(4) + '.pt')
      E = torch.load('data/Es_' + str(index).zfill(4) + '.pt')
      F = torch.load('data/Fs_' + str(index).zfill(4) + '.pt')
    else:
      R = torch.load('data/test_Rs_' + str(index).zfill(4) + '.pt')
      E = torch.load('data/test_Es_' + str(index).zfill(4) + '.pt')
      F = torch.load('data/test_Fs_' + str(index).zfill(4) + '.pt')
    return R, E, F
