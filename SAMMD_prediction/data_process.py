import numpy as onp
import jax.numpy as np
from jax import device_put
with open('silica_train.npz', 'rb') as f:
  files = onp.load(f)
  Rs, Es, Fs = [device_put(x) for x in (files['arr_0'], files['arr_1'], files['arr_2'])]
  for i in range(len(Rs)):
    torch.save(Rs[i],"data/Rs_"+str(i).zfill(4)+".pt")
    torch.save(Es[i],"data/Es_"+str(i).zfill(4)+".pt")
    torch.save(Fs[i],"data/Fs_"+str(i).zfill(4)+".pt")
  test_Rs, test_Es, test_Fs = [device_put(x) for x in (files['arr_3'], files['arr_4'], files['arr_5'])]
  for i in range(len(test_Rs)):
    torch.save(test_Rs[i],"data/test_Rs_"+str(i).zfill(4)+".pt")
    torch.save(test_Es[i],"data/test_Es_"+str(i).zfill(4)+".pt")
    torch.save(test_Fs[i],"data/test_Fs_"+str(i).zfill(4)+".pt")
  print(torch.load("data/Rs_0000.pt").shape)
  print(torch.load("data/Es_0000.pt"))
  print(torch.load("data/Fs_0000.pt").shape)
  test_Rs = test_Rs
  test_Es = test_Es
  test_Fs = test_Fs