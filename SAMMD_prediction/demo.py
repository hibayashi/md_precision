import itertools as it
from typing import Generator, NamedTuple, Tuple

import chex
import haiku
import jax
import jax.numpy as jnp
from jax import random
from jax_md import space
from jax_md.energy import graph_network
from jax import jit
import optax
from tqdm import tqdm
from dataset import Dataset
from jax import vmap
from torch.utils.data import DataLoader

from sam import look_sharpness_aware

jax.config.update("jax_debug_nans", True)


class FLAGS(NamedTuple):
    KEY = random.PRNGKey(0) 
    BATCH_SIZE = 32 
    NUM_WORKERS = 1

class TrainState(NamedTuple):
    params: optax.Params
    opt_st: optax.OptState
    loss: float
    cnt: int


class Batch(NamedTuple):
    R: chex.Array
    E: chex.Array
    F: chex.Array

training_set = Dataset(is_train=True)
test_set = Dataset(is_train=False)

training_generator = DataLoader(training_set,
                                batch_size=FLAGS.BATCH_SIZE,
                                shuffle=True,
                                num_workers=FLAGS.NUM_WORKERS)
test_generator = DataLoader(test_set,
                            batch_size=500,
                            shuffle=False,
                            num_workers=FLAGS.NUM_WORKERS)


def objective(params: optax.Params, batch: Batch) -> chex.Array:
    return jnp.mean((vectorized_energy_fn(params, batch.R) - batch.E) ** 2)

def optimizer(batch: Batch) -> optax.GradientTransformation:
    """
    Creates a sharpness-aware optimizer for a single step that closes over the
    given batch of input data.
    """

    def forward(params: optax.Params) -> optax.Updates:
        # IMPORTANT: inner forward pass and sharpness-aware ascent must
        # close over the same data used to perform the forward pass in the final
        # gradient update
        return jax.grad(objective)(params, batch)

    optim = optax.adam(1e-4)

    # SAM
    # return optax.chain(look_sharpness_aware(forward), optim)
    # no SAM
    return optax.chain(optim)

def cma_update(old: chex.Array, new: chex.Array, n: int) -> chex.Array:
    cma = n * old + new
    return cma / (n + 1)

@jit
def train_step(state: TrainState, batch: Batch) -> TrainState:
   "Performs a single training step."
   loss, grads = jax.jit(jax.value_and_grad(objective))(state.params, batch)
   grads, optst = optimizer(batch).update(grads, state.opt_st, state.params)
   params = optax.apply_updates(state.params, grads)
   loss = cma_update(state.loss, loss, state.cnt)
   return TrainState(params=params, opt_st=optst, loss=loss, cnt=state.cnt + 1)

@jit
def eval(state: TrainState, batch: Batch):
   loss, _ = jax.jit(jax.value_and_grad(objective))(state.params, batch)
   return loss

# Box
box_size = 10.862
displacement, shift = space.periodic(box_size)
# Network
init_fn, energy_fn = graph_network(displacement, r_cutoff=3.0)
vectorized_energy_fn = vmap(energy_fn, (None, 0))
# Initialization
batch = Batch(training_set[0][0], training_set[0][1], training_set[0][2])
params = init_fn(FLAGS.KEY, training_set[0][0])
opt_st = optimizer(batch).init(params)
state = TrainState(params, opt_st, jnp.array(0.0), 0)
# Training
total_epochs = 30
trajectory = jnp.zeros((0,3))
for epoch in range(total_epochs):
    train_loss = 0
    cnt = 0
    for Rs, Es, Fs in training_generator:
        batch = Batch(jnp.array(Rs), jnp.array(Es), jnp.array(Fs))
        train_loss = cma_update(train_loss, eval(state, batch), cnt)
        cnt += 1
    test_loss = 0
    cnt = 0
    for Rs, Es, Fs in test_generator:
        batch = Batch(jnp.array(Rs), jnp.array(Es), jnp.array(Fs))
        test_loss = cma_update(test_loss, eval(state, batch), cnt)
        cnt += 1
    print(f'Train Loss at Epoch {epoch} is {train_loss:.3g}')
    print(f'Test Loss at Epoch {epoch} is {test_loss:.3g}')
    trajectory = jnp.concatenate((trajectory, jnp.array([[epoch,train_loss,test_loss]])))
    for Rs, Es, Fs in training_generator:
        batch = Batch(jnp.array(Rs), jnp.array(Es), jnp.array(Fs))
        state = train_step(state, batch)
    state = TrainState(state.params, state.opt_st, jnp.array(0.0), 0)
jnp.save("results/trajectory", trajectory)
