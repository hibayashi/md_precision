import matplotlib.pyplot as plt
import seaborn as sns
import time
import warnings
import jax
from jax.example_libraries import stax
import jax.numpy as jnp
import optax

## Box properties
BOX_SIZE = 10.862

## Trainig properties
TRAIN_BATCH_SIZE = 5 
TRAIN_NUM_WORKERS = 1
EPOCHS = 10
LEARNING_RATE = 1e-4

## Test properties
TEST_BATCH_SIZE = 30 
TEST_NUM_WORKERS = 1

## SAM parameters
RHO = 0.05


# Defiing the simulation space
from jax_md import space

displacement_fn, shift_fn = space.periodic(BOX_SIZE)

from jax_md import energy
from jax import vmap
from jax import device_put

init_fn, energy_fn = energy.graph_network(displacement_fn, r_cutoff=3.0) 
vectorized_energy_fn = vmap(energy_fn,(None,0))


from torch.utils.data import Dataset, DataLoader

import glob
import os
import numpy as np

class REFData(Dataset):
    def __init__(self, fpath, is_train):
        self.root_dir = fpath # load root dir
        self.is_train=is_train        

    def __len__(self):
      """
        return total dataset size assuming no. of E, R, & F frames are the same
      """
      files = self.root_dir+'/test_Rs_*.pt'
      if self.is_train:
        files = self.root_dir+'/Rs_*.pt'      
      
      files_list=[f for f in glob.glob(files)]

      files_set = set(files_list)
      # print(len(files_set) - 4)
      return len(files_set)-10
      

    def __getitem__(self, index):              
        pos_file_name='Rs_'+str(index).zfill(4)+'.npy'
        pos_file_path = os.path.join(self.root_dir, pos_file_name)
        # print(pos_file_name,"is being accessed for positions")
        force_file_name='Fs_'+str(index).zfill(4)+'.npy'
        force_file_path = os.path.join(self.root_dir, pos_file_name)

        energy_file_name='Es_'+str(index).zfill(4)+'.npy'
        energy_file_path = os.path.join(self.root_dir, pos_file_name)
        
        Rs_numpy = np.load(pos_file_path)
        
        Fs_numpy = np.load(force_file_path)        

        Es_numpy = np.load(energy_file_path)
        
        return Rs_numpy, Fs_numpy, Es_numpy


# ## Get training set and test data set and create data generators for batch training

# In[7]:


training_set = REFData('./data/',True)

training_generator = DataLoader(
    training_set,
    shuffle=False,
    batch_size=TRAIN_BATCH_SIZE,    
    num_workers=TRAIN_NUM_WORKERS,    
)

testing_set = REFData('./data/',False)

testing_generator = DataLoader(
    testing_set,
    shuffle=False,
    batch_size=TEST_BATCH_SIZE,    
    num_workers=TEST_NUM_WORKERS,    
)


type(training_generator)


from jax import random

key = random.PRNGKey(0)


def energy_loss_fn(params,batch):  
  return jnp.mean((vectorized_energy_fn(params, batch['Rs']) - batch['Es']) ** 2)

def force_loss_fn(params,batch):  
  grad_fn = vmap(jax.grad(energy_fn, argnums=1), (None, 0))  
  return jnp.mean((grad_fn(params, batch['Rs']) + batch['Fs']) ** 2)

@jax.jit
def loss_fn(params,batch):  
#   return energy_loss_fn(params,batch) + force_loss_fn(params,batch)  
    return force_loss_fn(params,batch)  

from jax.tree_util import tree_flatten, tree_unflatten
from optax._src import numerics

tx = optax.adam(LEARNING_RATE)

@jax.jit
def train_step(params, opt_state, batch):

  grad_fn = jax.grad(loss_fn, has_aux=False)    

  grads = grad_fn(params, batch)
  updates, new_opt_state = tx.update(grads, opt_state, params)
  new_params = optax.apply_updates(params, updates)
  
  return new_params, new_opt_state

a = jnp.array([[1, 2], [3, 4]])
loss = jnp.mean(a)
print(jnp.array([loss])) # since out loss function returns a array, not a int or float type 
myList = []
myList.append(jnp.array([loss]))
myList.append(jnp.array([loss]))
myList.append(jnp.array([loss]))

print(jax.tree_util.tree_reduce(lambda x, y : x+y ,myList)/len(myList))
print(jax.tree_leaves(myList))

from jax.tree_util import tree_reduce
params = init_fn(key,  training_set[0][0])
opt_state = tx.init(params)

training_loss_over_epochs = []
testing_loss_over_epochs = []

print('\nStarting training...')
for epoch in range(1, EPOCHS + 1):
  training_loss_over_batches = []
  testing_loss_over_batches = []

  start_time = time.time()
  for training_data in training_generator:
    batch = {'Rs': jnp.array(training_data[0]), 'Fs': jnp.array(training_data[1]), 'Es': jnp.sum(jnp.array(training_data[2]), axis=[2,1]) }
    params, opt_state = train_step(params, opt_state, batch)
    print("batch loss: ", loss_fn(params,batch))
    training_loss_over_batches.append(loss_fn(params,batch)) # forms a list of JAX DeviceArray type objects, hence it is a pytree

    
  epoch_time = time.time() - start_time
  print(f'Epoch {epoch} in {epoch_time:0.2f} seconds.')
#   print("training loss over batches ", training_loss_over_batches)
#   training_loss_over_epochs.append(tree_reduce(lambda x, y: x+y,  training_loss_over_batches)) # get the average loss for all batches in an epoch

#   for test_data in testing_generator:
#     test_batch = {'Rs': jnp.array(test_data[0]), 'Fs': jnp.array(test_data[1]), 'Es': jnp.sum(jnp.array(test_data[2]), axis=[2,1]) }

#     testing_loss_over_batches.append(loss_fn(params,test_batch))      
    
#   testing_loss_over_epochs.append(tree_reduce(lambda x, y: x+y, testing_loss_over_batches))


print(EPOCHS)
print(training_loss_over_epochs)
plt.plot(range(EPOCHS), training_loss_over_epochs, 'o')
