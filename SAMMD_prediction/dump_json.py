import pickle
import json 
import numpy as np
with open('si_gnn.pickle', 'rb') as f:
  params = pickle.load(f)
def npdict_to_list(npdict):
  for key in npdict:
    if type(npdict[key]) is dict: 
      npdict[key] = npdict_to_list(npdict[key])
    elif type(npdict[key]) is np.ndarray:
      npdict[key] = npdict[key].tolist()
  return npdict
params = npdict_to_list(params)
with open('si_gnn.json', 'w') as outfile:
  json.dump(params, outfile)
