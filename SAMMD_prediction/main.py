from dataset import Dataset
import haiku as hk
from typing import NamedTuple
from jax_md import space
from jax_md.energy import graph_network
from jax import random
from jax import vmap
from jaxi import jit
import optax
from jax import grad
import jax.numpy as jnp
import torch
from torch.utils.data import DataLoader
from jax.lib import xla_bridge
import cProfile
import re

class FLAGS(NamedTuple):
    KEY = random.PRNGKey(0) 
    BATCH_SIZE = 32 
    NUM_WORKERS = 1

training_set = Dataset(is_train=True)
test_set = Dataset(is_train=False)
training_generator = DataLoader(training_set,
                                batch_size=FLAGS.BATCH_SIZE,
                                shuffle=True,
                                num_workers=FLAGS.NUM_WORKERS)
test_generator = DataLoader(test_set,
                            batch_size=500,
                            shuffle=False,
                            num_workers=FLAGS.NUM_WORKERS)

# Box
box_size = 10.862
displacement, shift = space.periodic(box_size)
# Network
init_fn, energy_fn = graph_network(displacement, r_cutoff=3.0)
params = init_fn(FLAGS.KEY, training_set[0][0])
vectorized_energy_fn = vmap(energy_fn, (None, 0))
opt = optax.chain(optax.clip_by_global_norm(0.01), optax.adam(1e-4))
opt_state = opt.init(params)

@jit
def update(params, opt_state, batch):
  R, energy, force = batch   
  def energy_loss_fn(params):
    return jnp.mean((vectorized_energy_fn(params, R) - energy) ** 2)
  
  def force_loss_fn(params):
    # We want the gradient with respect to the position, not the parameters.
    grad_fn = vmap(grad(energy_fn, argnums=1), (None, 0))
    return jnp.mean((grad_fn(params, R) + force) ** 2)
  
  def loss_fn(params):
    return energy_loss_fn(params) + force_loss_fn(params)
  updates, opt_state = opt.update(grad(loss_fn)(params), opt_state)
  return optax.apply_updates(params, updates), opt_state, loss_fn(params)

@jit
def eval(params, batch):
  R, energy, force = batch   
  def energy_loss_fn(params):
    return jnp.mean((vectorized_energy_fn(params, R) - energy) ** 2)
  
  def force_loss_fn(params):
    # We want the gradient with respect to the position, not the parameters.
    grad_fn = vmap(grad(energy_fn, argnums=1), (None, 0))
    return jnp.mean((grad_fn(params, R) + force) ** 2)
  
  def loss_fn(params):
    return energy_loss_fn(params) + force_loss_fn(params)
  return  loss_fn(params)

trajectory = jnp.zeros((0,3))
for epoch in range(30):
  train_loss = 0
  cnt = 0
  for Rs, Es, Fs in training_generator:
    batch = (jnp.array(Rs), jnp.array(Es), jnp.array(Fs))
    params, opt_state, loss = update(params, opt_state, batch)
    train_loss += loss
    cnt += 1
  train_loss = train_loss / cnt
  test_loss = 0
  cnt = 0
  for Rs, Es, Fs in test_generator:
    batch = (jnp.array(Rs), jnp.array(Es), jnp.array(Fs))
    test_loss += eval(params, batch)
    cnt += 1
  test_loss = test_loss / cnt
  print(f'Train Loss at Epoch {epoch} is {train_loss}')
  print(f'Test Loss at Epoch {epoch} is {test_loss}')
  trajectory = jnp.concatenate((trajectory, jnp.array([[epoch,train_loss,test_loss]])))
jnp.save("results/trajectory", trajectory)